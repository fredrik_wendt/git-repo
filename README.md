# git-repo

Detta är en README-fil, som skall läsas. Den visas ofta som del av ett git-förråds
"förstasida" och innehåller lite övergripande information om innehållet i 
förrådet. 


# 1. Personer som petat på denna fil

Ja, vi gör det enkelt. Klona ut detta repo (svenska: förråd), skapa en fil med
ditt användarnamn (undvik åäö) och skriv in ditt namn i filen. Exempel: 
`frewen.txt` med innehållet `Fredrik Wendt`.

När du redigerat filen, är det dags att registrera (spara) dina ändringar, dvs
den nya versionen av källkodsförrådet, i git. Gå tillbaka till din git-klient,
be git spåra filen, och committa slutligen din ändring. (Svensk översättning 
av "commit" finns egentligen inte, och "kommitta" är helt enkelt vedertaget
språkbruk.) 

`git clone https://github.com/FredrikWendt/skoj-repo.git`

`git add`

`git commit`


# 2. Git - decentraliserad versionshantering

En ändring som du sparat i ditt versionsförråd, dvs din commit, är lokal - den
finns och syns bara på din dator. För att dela ändringar behöver man skicka den
till andra datorer, t ex till servrarna bakom github.com. Detta görs genom att 
pusha (svenska: knuffa) sin ändringar.

`git push`

Git är designat för att vara snabbt att jobba med lokalt. Man behöver inte ha
tillgång till en server för att versionkontrollera den källkod man har på sin 
dator. CVS, Subversion och Microsofts TFS (äldre) var tvärtom skapade för att 
(nästan) alltid prata med en server när man ville utföra en ändring. Det gjorde
det svårt att jobba när man var frånkopplad (flyg, tåg, hemma). Vidare blev det 
långsamt och det var svårt att få servrar att hantera belastningen från många
samtidiga utvecklare. Git skalar mycket bättre (men har också gränser).


## Varje klon av ett förråd, är en komplett kopia över hela förrådets historik.

Därför kan en lokalt, enkelt, göra alla typer av operationer - såsom att leta
efter obskyra buggar som kom "någonstans mellan ändring 1000 och 1250". 

Disk är billigt. Bandbredd är billigt. CPU är billigt.


## Git-förråd är kryptografiskt korrekta acykliska träd av ändringar

`git log --graph`

* file - hash
* tree
* commit


# Pull request

Istället för push kan man göra ändring, och erbjuda den uppströms: pull request.

`git pull`

Skapar en `fork`


# CONFLICT: Merge & Rebase

`git merge`

`git rebase`

`git status`


# Branching

Lokalt, billigt. Dyrt ur processsynpunkt. "Done"?


# Continuous Integration

- pre-commit quality gate
- versionskontrollera pipeline
- "does it compile? can it install?"


# Continuous Delivery

- publicera installationsfilen
- "will it upgrade, rollback, ...?"


# Continuous Deployment

- ändringar blir live direkt
- "do users like it?"


## Deployment Patterns

# DevOps

